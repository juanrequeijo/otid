export class Icons {
  public static list = [
    {
      'name': 'calendar',
      'path': 'assets/icons/calendar.svg'
    },
    {
      'name': 'check2',
      'path': 'assets/icons/check.svg'
    },
    {
      'name': 'clock',
      'path': 'assets/icons/clock.svg'
    },
    {
      'name': 'money',
      'path': 'assets/icons/money.svg'
    }
    ,
    {
      'name': 'place',
      'path': 'assets/icons/place.svg'
    }
  ]
}