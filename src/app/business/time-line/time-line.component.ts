import { Component, OnInit } from '@angular/core';
import { identifierModuleUrl } from '@angular/compiler';
import { environment } from 'src/environments/environment'
import { RequestService } from 'src/app/services/request.service';


@Component({
  selector: 'app-time-line',
  templateUrl: './time-line.component.html',
  styleUrls: ['./time-line.component.scss']
})
export class TimeLineComponent implements OnInit {

  public title: String = 'dito';

  public dataMock = {
    "events": [
      {
        "event": "comprou-produto",
        "timestamp": "2016-09-22T13:57:32.2311892-03:00",
        "custom_data": [
          {
            "key": "product_name",
            "value": "Camisa Azul"
          }, {
            "key": "transaction_id",
            "value": "3029384"
          }, {
            "key": "product_price",
            "value": 100
          }
        ]
      }, {
        "event": "comprou",
        "timestamp": "2016-09-22T13:57:31.2311892-03:00",
        "revenue": 250,
        "custom_data": [
          {
            "key": "store_name",
            "value": "Patio Savassi"
          }, {
            "key": "transaction_id",
            "value": "3029384"
          }
        ]
      }, {
        "event": "comprou-produto",
        "timestamp": "2016-09-22T13:57:33.2311892-03:00",
        "custom_data": [
          {
            "key": "product_price",
            "value": 150
          }, {
            "key": "transaction_id",
            "value": "3029384"
          }, {
            "key": "product_name",
            "value": "CalÃ§a Rosa"
          }
        ]
      }, {
        "event": "comprou-produto",
        "timestamp": "2016-10-02T11:37:35.2300892-03:00",
        "custom_data": [
          {
            "key": "transaction_id",
            "value": "3409340"
          }, {
            "key": "product_name",
            "value": "Tenis Preto"
          }, {
            "key": "product_price",
            "value": 120
          }
        ]
      }, {
        "event": "comprou",
        "timestamp": "2016-10-02T11:37:31.2300892-03:00",
        "revenue": 120,
        "custom_data": [
          {
            "key": "transaction_id",
            "value": "3409340"
          }, {
            "key": "store_name",
            "value": "BH Shopping"
          }
        ]
      }
    ]
  }
  public transations: Array<{}>;
  public products: any;
  private store: string;
  private data;

  constructor(
    private request: RequestService,
  ) {
    this.transations = [];
    this.products = [];
    this.store = environment.store
  }

  ngOnInit() {
    this.fetchData();
  }

  buildData(data) {
    data.events.forEach(event => {
      if (event.event === 'comprou') {
        this.transations.push(event);
      } else if (event.event === 'comprou-produto') {
        this.products.push(event);
      }
    });
  };

  fetchData() {
    // this.request.getERR(this.store).then(data => {
    //   this.fun(data);
    // });
    // err em requisição CORS em FILE
    this.buildData(this.dataMock);
  }

  getProducts(t: any) {
    const id = t.custom_data[0].key === 'transaction_id' ? t.custom_data[0].value : t.custom_data[1].value;
    return this.products.filter(p => p.custom_data.filter(_p => _p.key === 'transaction_id')[0].value === id);
  }

  getProduct(p) {
    const obj = p.custom_data.filter(_p => _p.key === "product_name");
    return obj[0].value;
  }

  getPrice(p) {
    const obj = p.custom_data.filter(_p => _p.key === "product_price");
    return obj[0].value;
  }

}
