import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TimeLineComponent } from './business/time-line/time-line.component';
import { PageNotFoundComponent } from './business/page-not-found/page-not-found.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AppRouting } from './app-routing';
import { DashboardComponent } from './business/dashboard/dashboard.component';

import { MglTimelineModule } from 'angular-mgl-timeline';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    TimeLineComponent,
    PageNotFoundComponent,
    NavbarComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatIconModule,
    AppRouting,
    MglTimelineModule,
    BrowserAnimationsModule
  ],
  exports: [
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
