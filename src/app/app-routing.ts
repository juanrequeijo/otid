import { Routes, RouterModule } from '@angular/router';
import { TimeLineComponent } from './business/time-line/time-line.component';
import { PageNotFoundComponent } from './business/page-not-found/page-not-found.component';
import { DashboardComponent } from './business/dashboard/dashboard.component';

const routes: Routes = [
    {
      path: '',
      component: DashboardComponent
    },
    {
        path: 'time-line',
        component: TimeLineComponent
    },
    {
        path: 'not-found',
        component: PageNotFoundComponent
    }
];

export const AppRouting = RouterModule.forRoot(routes);